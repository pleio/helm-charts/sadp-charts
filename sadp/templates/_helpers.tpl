{{/*
The name of the release, used as label or a base for other names.
*/}}
{{- define "sadp.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{/*
The name used to labels resources associated with the web deployment.
*/}}
{{- define "sadp.webName" -}}
{{- printf "%s-web" (include "sadp.name" . )  -}}
{{- end }}

{{/*
The name used to labels resources associated with the admin deployment.
*/}}
{{- define "sadp.adminName" -}}
{{- printf "%s-admin" (include "sadp.name" . )  -}}
{{- end }}

{{/*
The name used to labels resources associated with the mock IDP deployment.
*/}}
{{- define "sadp.mockIdpName" -}}
{{- printf "%s-mock-idp" (include "sadp.name" . )  -}}
{{- end }}

{{/*
The name used to labels resources associated with the NGINX webserver deployment.
*/}}
{{- define "sadp.webserverName" -}}
{{- printf "%s-nginx" (include "sadp.name" . ) -}}
{{- end }}

{{/*
The name used to labels resources associated with the mock NGINX webserver deployment.
*/}}
{{- define "sadp.mockWebserverName" -}}
{{- printf "%s-mock-nginx" (include "sadp.name" . ) -}}
{{- end }}

{{/*
The name of the secret containing the keypair used to encrypt SAML traffic.
Through our metadata endpoint, this certificate is provided to external parties.
*/}}
{{- define "sadp.keypairSecretName" -}}
{{- printf "%s-keypair" (include "sadp.name" . ) -}}
{{- end }}

{{/*
The root CA to trust for the external API endpoint related to the BSN checker and other APIs.
*/}}
{{- define "sadp.rootCaSecretName" -}}
{{- printf "%s-root-ca" (include "sadp.name" . ) -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing media files, i.e. images and documents.
*/}}
{{- define "sadp.mediaSharedStoragePvcName" -}}
{{- printf "%s-media" (include "sadp.name" . ) -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing file uploads, i.e. files uploaded for digital appeals.
*/}}
{{- define "sadp.fileUploadsSharedStoragePvcName" -}}
{{- printf "%s-file-uploads" (include "sadp.name" . ) -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing encrypted files.
*/}}
{{- define "sadp.encryptedFilesSharedStoragePvcName" -}}
{{- printf "%s-encrypted-files" (include "sadp.name" . ) -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing Huey related data.
*/}}
{{- define "sadp.hueySharedStoragePvcName" -}}
{{- printf "%s-huey" (include "sadp.name" . ) -}}
{{- end }}

{{/*
A set of standard labels as defined by the Helm developers to use in a chart.
*/}}
{{- define "sadp.standardLabels" -}}
app.kubernetes.io/name: {{ include "sadp.name" . }}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
app.kubernetes.io/managed-by: {{ .Release.Service }}	
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "sadp.selectorLabels" -}}
app.kubernetes.io/name: {{ include "sadp.name" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}	
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "sadp.backgroundSchedulerName" -}}
{{- printf "%s-background-scheduler" (include "sadp.name" .) -}}
{{- end -}}
