# SADP Charts

This repository contains the Helm-chart for [SADP](https://gitlab.com/pleio/dossier-sadp) (and any future charts which are related to this project). 

## Installation
See [the SADP Chart's README](./sadp/README.md) for installation instructions

## Chart Releases
Any changes in the chart directories (e.g. [sadp](./sadp/)) triggers a pipeline which publishes a chart package into this project's [package registry](https://gitlab.com/pleio/helm-charts/sadp-charts/-/packages). 

## Development
Please create merge-requests for any additional features or bugfixes. Please do not forget to:
* bump the chart-version in the Chart.yaml (so `version`, `appversion` can stay as is)
* add any additional values in de [SADP chart README](./sadp/README.md) and run [helm-docs](https://github.com/norwoodj/helm-docs/tree/master) to automatically generate the docs.